const express = require('express');
const User = require('../models/User');

const router = express.Router();

router.post('/', async(req, res) => {
    if(!req.body.username || !req.body.password) {
        return res.status(400).send('Data not valid');
    }
    const userData = {
        username: req.body.username,
        password: req.body.password
    };

    const user = new User(userData);

    try{
        user.generateToken();
        await user.save();
        res.send(user);
    }catch{
        res.sendStatus(500);
    }
});


router.post('/session', async(req, res) => {
    try{
        const user = await User.findOne({username: req.body.username});
        if(!user){
            res.status(401).send({error: 'User not found'});
        }

        const isMatch = await user.checkPassword(req.body.password);
        if(!isMatch){
            res.status(401).send({error: 'Password is wrong'});
        }


        user.generateToken();
        console.log(user)
        await user.save();
        res.send({token: user.token});

    }catch{
        res.sendStatus(500);
    }
});

module.exports = router;

