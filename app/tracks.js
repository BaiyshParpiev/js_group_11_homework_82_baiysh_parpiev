const express = require('express');
const Tracks = require('../models/Track');

const router = express.Router();

router.get('/', async(req, res) => {
    try{
        const tracks = await Tracks.find()
        res.send(tracks)
    }catch{
        res.send(500)
    }
});

router.post('/', async(req, res) => {
    if(!req.body.name || !req.body.album){
        res.status(404).send('Data not valid');
    }

    const trackData = {
        name: req.body.name,
        album: req.body.album,
        duration: req.body.duration || null
    };

    const track = new Tracks(trackData);
    try{
        await track.save();
        res.send(track);
    }catch(e){
        res.status(400).send(e)
    }
});

module.exports = router;