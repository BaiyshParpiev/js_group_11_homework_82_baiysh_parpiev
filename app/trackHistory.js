const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require("../models/User");

const router = express.Router();

router.post('/', async(req, res) => {
    const token = req.get('Authorization');
    console.log(token)
    if(!token){
        res.status(401).send({error: 'Unauthorized'});
    }

    if(!req.body.track){
        res.status(400).send('Data not valid')
    }

    const user = await User.findOne({token});
    if(!user){
        return res.status(401).send({error: 'Wrong token'});
    }

    const trackHistoryData = {
        track: req.body.track,
        user: user._id,
        datetime: new Date().toLocaleString()
    };

    const trackHistory = new TrackHistory(trackHistoryData);
    try{
        await trackHistory.save();
        res.send(trackHistory);
    }catch{
        res.sendStatus(500)
    }
});

module.exports = router;
