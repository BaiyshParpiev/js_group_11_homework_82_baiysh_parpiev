const express = require('express');
const Albums = require('../models/Album');
const path = require("path");
const multer = require('multer');
const {nanoid} = require('nanoid');
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});


const upload = multer({storage});

const router = express.Router();

router.get('/', async(req, res) => {
    try{
        const query = {};
        if(req.query.artist){
            query.artist = req.query.artist
        }
         const albums = await Albums.find(query);
        res.send(albums)
    }catch{
        res.sendStatus(500);
    }
});

router.get('/:id', async(req, res) => {
    try{
        const album = await Albums.findOne({_id: req.params.id}).populate('artist', 'name description');
         if(album){
             res.send(album);
         }else{
             res.status(404).send('Data not valid');
        }
    }catch{
        res.sendStatus(500);
    }
});

router.post('/', upload.single('image'), async(req, res) => {
    if(!req.body.name || !req.body.artist || !req.body.datetime){
        return res.status(404).send('Data not valid');
    }

    const albumData = {
        name: req.body.name,
        artist: req.body.artist,
        datetime: req.body.datetime,
        image: req.body.image || null
    }

    const album = new Albums(albumData);
    try{
        await album.save();
        res.send(album)
    }catch(e){
        res.status(400).send(e);
    }
});

module.exports = router;
