const express = require('express');
const Artists = require('../models/Artist');
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});


const router = express.Router();

router.get('/', async(req, res) => {
    try{
        const artists = await Artists.find();
        res.send(artists)
    }catch{
        res.sendStatus(500);
    }
});

router.post('/', upload.single('image'), async(req, res) => {
    if(!req.body.name){
        return res.status(404).send('Data not valid');
    }

    const artistData = {
        name: req.body.name,
        image: req.body.image || null,
        description: req.body.description || null
    };

    const artist = new Artists(artistData)
    try{
        await artist.save();
        res.send(artist)
    }catch(e){
        res.status(400).send(e);
    }
});

module.exports = router;
