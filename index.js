const express = require('express');
const cors = require('cors');
const exitHook = require('async-exit-hook');
const album = require('./app/albums');
const artist = require('./app/artists');
const track = require('./app/tracks');
const users = require('./app/users');
const trackHistory = require('./app/trackHistory');
const mongoose = require('mongoose');

const app = express();

app.use(express.json());
app.use(cors());
app.use(express.static('public'));
const port = 8000;

app.use('/albums', album);
app.use('/artists', artist);
app.use('/tracks', track);
app.use('/users', users);
app.use('/track_history', trackHistory)


const run = async() => {
    await mongoose.connect('mongodb://localhost/tracks')
    app.listen(port, () => {
        console.log('Server start on ', port, '!')
    });

    exitHook(() => {
        console.log('End of DB');
        mongoose.disconnect();
    });
};

run().catch(e => console.error(e))

